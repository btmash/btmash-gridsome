// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const slugify = require('@sindresorhus/slugify')
const fs = require('fs');
const path = require('path')
const axios = require('axios')

module.exports = function (api) {
  api.loadSource(({ addCollection }) => {
    // Use the Data Store API here: https://gridsome.org/docs/data-store-api/
  })

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  })

  api.onCreateNode(options => {
    if (options.internal.typeName == 'DrupalFile') {
      const drupalSource = process.GRIDSOME.plugins._plugins.filter(function(item) {
        return item.api._entry.name == 'DrupalSource'
      })
      let drupalOptions = drupalSource[0].entry.options
      drupalOptions.requestConfig.responseType = 'stream'
      let dir = __dirname + '/static' + path.dirname(options.uri.url)
      !fs.existsSync(dir) && fs.mkdirSync(dir, {recursive: true});
      let fileUrl = drupalOptions.baseUrl + options.uri.url
      let destination = __dirname + '/static' + options.uri.url
      axios.get(fileUrl, drupalOptions.requestConfig)
        .then(response => {
          response.data.pipe(fs.createWriteStream(destination))
        })
      delete drupalOptions.requestConfig.responseType
      options.gridsome_url = "~/static" + options.uri.url
    }

    if (options.internal.typeName == 'DrupalNodeArticle') {
      let drupalpath = options.drupalpath.alias.split('/')
      options.date_path = drupalpath[2]
      options.title_path = drupalpath[3]
      options.path = '/article/' + options.date_path + '/' + options.title_path

      let content = options.body.value
      content = content.replace('<code', '<vue-code-highlight')
      content = content.replace('</code', '</vue-code-highlight')
      options.content = content
    }
    else if (options.internal.typeName == 'DrupalTaxonomyTermTags') {
      options.path = '/tag/' + slugify(options.name)
    }
    else {
      options.path = '/' + options.internal.typeName + '/' + slugify(options.id)
    }
  })
}
