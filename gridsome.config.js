// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

let excludes = [
  'self',
  'block--block',
  'block_content--basic',
  'block_content_type--block_content_type',
  'comment_type--comment_type',
  'comment--comment',
  'comment--comment_node_article',
  'comment--comment_node_test_content_type',
  'contact_form--contact_form',
  'contact_message--feedback',
  'contact_message--personal',
  'contact_message--website_feedback',
  'editor--editor',
  'field_storage_config--field_storage_config',
  'field_config--field_config',
  'filter_format--filter_format',
  'rdf_mapping--rdf_mapping',
  'rest_resource_config--rest_resource_config',
  'search_page--search_page',
  'shortcut--default',
  'shortcut_set--shortcut_set',
  'action--action',
  'tour--tour',
  'user_role--user_role',
  'view--view',
  'date_format--date_format',
  'base_field_override--base_field_override',
  'entity_form_mode--entity_form_mode',
  'entity_view_mode--entity_view_mode',
  'entity_view_display--entity_view_display',
  'entity_form_display--entity_form_display',
  'taxonomy_vocabulary--taxonomy_vocabulary',
];

module.exports = {
  siteName: 'BTMash',
  siteDescription: 'Blob of contradictions',

  plugins: [
    {
      use: '@gridsome/source-drupal',
      options: {
        baseUrl: process.env.DRUPAL_BASE_URL,
        apiBase: process.env.DRUPAL_API_BASE,
        requestConfig: {
          auth: {
            username: process.env.BASIC_AUTH_USERNAME,
            password: process.env.BASIC_AUTH_PASSWORD
          }
        },
        templates: {
          drupalNodeArticle: [
            {
              name: "article",
              path: 'article/:date_path/:title_path'
            },
            {
              name: "direct-node-link",
              path: 'node/:drupal_internal_nid'
            }
          ],
          drupalTaxonomyTermTags: [
            {
              name: "tag",
              path: 'article/:title'
            }
          ],
        },
        exclude: excludes
      }
    }
  ],

  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      externalLinksTarget: '_blank',
      externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
      anchorClassName: 'icon icon-link',
      plugins: [
        '@gridsome/remark-prismjs'
      ]
    }
  }

}
